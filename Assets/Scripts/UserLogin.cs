﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UserLogin : MonoBehaviour
{
    //Make sure to attach these Buttons in the Inspector
    public Button m_loginButton;
	
	// Start is called before the first frame update
    void Start()
    {
        //Calls the TaskOnClick/TaskWithParameters/ButtonClicked method when you click the Button
        m_loginButton.onClick.AddListener(TaskOnClick);
        m_loginButton.onClick.AddListener(delegate {TaskWithParameters("Hello"); });
        m_loginButton.onClick.AddListener(() => ButtonClicked(42));

    }

    void TaskOnClick()
    {
        //Output this to console when Button is clicked
        Debug.Log("You have clicked the button!");
    }

    void TaskWithParameters(string message)
    {
        //Output this to console when the Button is clicked
        Debug.Log(message);
    }

    void ButtonClicked(int buttonNo)
    {
        //Output this to console when the Button is clicked
        Debug.Log("Button clicked = " + buttonNo);
    }
}
